#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 195  207  201 }



historical_idea_groups = {
	economic_ideas
	offensive_ideas
	influence_ideas
	defensive_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Generic Rzentur Names
	"Drhol #0" = 50
	"Drholko #0" = 50
	"Olar #0" = 50
	"Aron #0" = 50
	"Oldir #0" = 50
	"Alvor #0" = 50
	"Ondral #0" = 50
	"Aranthiv #0" = 50
	"Ardjod #0" = 50
	"Ardortija #0" = 50
	"Asatur #0" = 50
	"Orfej #0" = 50
	"Arkinej #0" = 50
	"Vartijen #0" = 50
	"Borthid #0" = 50
	"Morsu #0" = 50
	"Ladjesan #0" = 50
	"Karlesar #0" = 50
	"Kesandor #0" = 50
	"Kalomar #0" = 50
	"Karrodej #0" = 50
	"Caevalk #0" = 50
	"Korej #0" = 50
	"Kandej #0" = 50
	"Kalrod #0" = 50
	"Celajan #0" = 50
	"Celtija #0" = 50
	"Celantija #0" = 50
	"Dracnes #0" = 50
	"Dajavor #0" = 50
	"Demivor #0" = 50
	"Dorentija #0" = 50
	"Ebor #0" = 50
	"Elantija #0" = 50
	"Elijod #0" = 50
	"Elezhar #0" = 50
	"Elezhod #0" = 50
	"Eldron #0" = 50
	"Egorandir #0" = 50
	"Rolandir #0" = 50
	"Erol #0" = 50
	"Erendej #0" = 50
	"Orland #0" = 50
	"Jevon #0" = 50
	"Filinej #0" = 50
	"Fynnd #0" = 50
	"Galnol #0" = 50
	"Galindej #0" = 50
	"Galond #0" = 50
	"Galekir #0" = 50
	"Galmon #0" = 50
	"Gospodej #0" = 50
	"Ossard #0" = 50
	"Ibbenej #0" = 50
	"Ivoj #0" = 50
	"Ivandej #0" = 50
	"Ilror #0" = 50
	"Ahor #0" = 50
	"Aholor #0" = 50
	"Jecmenavalk #0" = 10 #commoner name
	"Vojanser #0" = 50
	"Kir #0" = 50
	"Lovko #0" = 50
	"Vojmir #0" = 50
	"Munadej #0" = 50
	"Mondor #0" = 50
	"Morzhu #0" = 10 #commoner name
	"Mordrej #0" = 50
	"Nestemen #0" = 50
	"Olor #0" = 50
	"Payoko #0" = 30 #epednar origin
	"Belor #0" = 50
	"Peljod #0" = 50
	"Privladr #0" = 50
	"Ron #0" = 50
	"Selrevjan #0" = 50
	"Serondor #0" = 50
	"Saldurej #0" = 50
	"Strodtija #0" = 50
	"Stroddej #0" = 50
	"Telor #0" = 50
	"Telija #0" = 50
	"Telirjod #0" = 50
	"Zaravoj #0" = 50
	"Zurantija #0" = 50
	"Zurvjod #0" = 50
	"Threndor #0" = 50
	"Zirnej #0" = 50
	"Bentrjon #0" = 50
	"Hrobilan #0" = 50
	"Drozmabeas #0" = 50
	"Threthvor #0" = 50
#	"Trantan #0" = 50 #Only for Vizkaladr
	"Tranvor #0" = 50
	"Rzavor #0" = 50
	"Uvor #0" = 50
	"Gacerod #0" = 50
	"Varamen #0" = 50
	"Vajamor #0" = 50
	"Garamej #0" = 50
	"Garmelon #0" = 50
	"Van #0" = 50
	"Gardevoj #0" = 50
	"Valadrhol #0" = 50
	"Vindej #0" = 50
	"Miusard #0" = 50
	"Vojsard #0" = 50
	"Irvor #0" = 50
	"Yodzma #0" = 50
	"Zlotko #0" = 50
	
	"Adriona #0" = -20
	"Alarica #0" = -20
	"Alarisa #0" = -20
	"Alejana #0" = -20
	"Alona #0" = -20
	"Amaredha #0" = -20
	"Arioda #0" = -20
	"Calajana #0" = -20
	"Camnava #0" = -20
	"Celadeva #0" = -20
	"Devejana #0" = -20
	"Ebonika #0" = -20
	"Elethen #0" = -20
	"Elona #0" = -20
	"Eretka #0" = -20
	"Erlanvel #0" = -20
	"Filinava #0" = -20
	"Galinvel #0" = -20
	"Imaretka #0" = -20
	"Isehnika #0" = -20
	"Isenika #0" = -20
	"Iseranvel #0" = -20
	"Istranika #0" = -20
	"Ivranica #0" = -20
	"Ivranjana #0" = -20
	"Jexava #0" = -20
	"Ladrinvel #0" = -20
	"Lanahava #0" = -20
	"Lanalora #0" = -20
	"Lelietka #0" = -20
	"Leslinvel #0" = -20
	"Lianlija #0" = -20
	"Merisa #0" = -20
	"Mihtrisa #0" = -20
	"Naretka #0" = -20
	"Nathajana #0" = -20
	"Seluzia #0" = -20
	"Serova #0" = -20
	"Shaernika #0" = -20
	"Sharajana #0" = -20
	"Sharinvel #0" = -20
	"Sherisa #0" = -20
	"Tanelana #0" = -20
	"Thalanvel #0" = -20
	"Valanna #0" = -20
	"Varilvana #0" = -20
	"Varinna #0" = -20
	"Varivana #0" = -20
	"Vehanika #0" = -20
	"Vehona #0" = -20
	"Vesiava #0" = -20
	"Zalisa #0" = -20
	
}

leader_names = {
	"yod Volak" Yodzma "yod Arserynn" "yod Vecno" "yod Hrelab" "yod Stairdremov" "yod Stairlren" "yod Stayagozhar" "yod Sibek"
	"iz Svemel" "iz Pomvasonn" "iz Gomod" "iz Juzondezan" "iz Urthid" "iz Gozhar" "iz Ynn" "iz Atrad" "iz Tabi" "iz Tur" "iz Starbacin"
	Bojev Borec Brancad Drozmabojev Drozmavitez Jaok Konjek Mach Modna Ogejnb Poazi Revalk Sengha Sne Sulic Tur Vogh Zhulic
	
	Blagaj Cnezer Drashkov Egon Frankapan Gorjan Horvat Ilok Jelacid Kacir Kaniski Keglev Kruzid Lakoj Milvoj Nelpik Ogejnid Olarid Pejacev Radiv Revalk Rukav Talovac Urjavalk Valkrder Velrej Vrancid Zrinski
}

ship_names = {
	Cres
	Goli
	Krl
	Pag Plavnik
	Susak
	Tijat
	Unije
	Zlarin Zmajan
}

army_names = {
	"Gomosenghan Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Gomosenghan Fleet"
}