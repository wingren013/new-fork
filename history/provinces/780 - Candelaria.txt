# No previous file for Candelaria
culture = farrani
religion = regent_court
capital = ""
tribal_owner = B23
hre = no

base_tax = 1
base_production = 1
base_manpower = 3

trade_goods = unknown

native_size = 78
native_ferocity = 9
native_hostileness = 9

add_permanent_province_modifier = {
	name = half_orcish_minority_oppressed_small
	duration = -1
}
add_permanent_province_modifier = {
	name = orcish_minority_oppressed_large
	duration = -1
}